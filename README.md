--------------------------------------------------------------------------------------------------------------------
# Protocolo para la Adquisición y el Almacenamiento de Señales Biomédicas Asociadas a la Actividad Vocal

## Bioing. Ariel E. Stassi - <aestassi@ingenieria.uner.edu.ar> 

## Dr. Gabriel Alzamendi - <galzamendi@ingenieria.uner.edu.ar> 

## Bioing. Juan M. Miramont - <jmiramont@ingenieria.uner.edu.ar> 

## Dr. Juan Felipe Restrepo - <jrestrepo@ingenieria.uner.edu.ar> 

*2020-02-18*

--------------------------------------------------------------------------------------------------------------------

## Abstract/Resumen
El presente documento establece  una serie de instrucciones y consideraciones,  necesarias para  la adquisición y el
almacenamiento de señales biomédicas asociadas a la actividad vocal,  provenientes de un sujeto en el interior de la
cámara anecoica de la  FIUNER.  El  objetivo  es  sistematizar  el  registro  simultáneo  de  las  señales de voz y
electroglotograma, correspondientes a la serie de emisiones presentada.

## Archivos
1. Protocolo:
    1. Generalidades 
    2. Comunicación 
    3. Configuración Hardware y Software 
    4. Interfaz 
    5. Emisiones 
2. Folleto Informativo 
3. Consentimiento Informado 
4. Cuestionario 
5. Lista de Emisiones 

--------------------------------------------------------------------------------------------------------------------
## Notas:

--------------------------------------------------------------------------------------------------------------------
## Log:

#### 2020-02-18
- Creación del repositorio

--------------------------------------------------------------------------------------------------------------------
Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>
