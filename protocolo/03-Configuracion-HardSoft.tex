%===================================================================================================================
%===================================================================================================================
%                                 PROTOCOLO PARA LA ADQUISICIÓN Y EL ALMACENAMIENTO
%                                DE SEÑALES BIOMÉDICAS ASOCIADAS A LA ACTIVIDAD VOCAL
%-------------------------------------------------------------------------------------------------------------------
%           CAPÍTULO TRES: INSTRUCCIONES PARA LA CONEXIÓN Y CONFIGURACIÓN DEL EQUIPAMIENTO DE ADQUISICIÓN
%-------------------------------------------------------------------------------------------------------------------
%...................................................................................................................
% Autores:
%   Ariel Stassi <aestassi@ingenieria.uner.edu.ar>
%   Gabriel Alzamendi <galzamendi@ingenieria.uner.edu.ar>
%   Juan Manuel Miramont <jmiramont@ingenieria.uner.edu.ar>
%   Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>
% 2020-02-14
%===================================================================================================================
%===================================================================================================================
\pagestyle{main}
\chapter{Instrucciones para la conexión y configuración del equipamiento de adquisición}
\label{proto2}
%===================================================================================================================
%                                                     OBJETIVOS
%===================================================================================================================
\section{Objetivos}
Los objetivos principales de este documento son:
\begin{itemize}
  \item Realizar  una lista de  los materiales necesarios  para la correcta  adquisición de señales  biomédicas --en
  particular, voz y electroglotograma--, provenientes de un sujeto ubicado en el interior de la cámara anecoica.
  \item  Proveer  un  método secuencial  para  el  montaje,  la configuración  y  el  ajuste  de  los  equipos  y el
  \emph{software}  de  manejo asociado,  que  permitan  la correcta  adquisición  simultánea  de  señales  de  voz y
  electroglotograma.
\end{itemize}
%===================================================================================================================
%                                                      ALCANCE
%===================================================================================================================
\section{Alcance}
El presente  documento ha sido  desarrollado para su utilización  en las instalaciones  de la cámara  anecoica de la
\fiuner,  con el equipamiento provisto por el \lsydnl,  perteneciente a la misma institución.  La cámara anecoica de
la \fiuner\ cuenta con dos recintos:  la cámara anecoica  propiamente dicha y la sala de comandos asociada.  En este
contexto,  se  ha  diseñado  el  siguiente  instructivo  con  el  objetivo  de  sistematizar  la  configuración  del
\emph{hardware} y el \emph{software} con que se cuenta,  para la correcta adquisición simultánea de señales de voz y
electroglotograma --en adelante, EGG--.
%===================================================================================================================
%                                               MATERIALES NECESARIOS
%===================================================================================================================
\section{Materiales necesarios}
\begin{itemize}
    \item 1 sistema de adquisición \biopac\ MP150 con los módulos accesorios UIM100C, DA100C y TCI115;
    \item \emph{Software} de comando de la adquisición \emph{AcqKnowledge 4.2};
    \item 1 electroglotógrafo \laryngograph\ EGG-A100, con cable paciente y cable de salida incluidos;
    \item 1 micrófono dinámico de alta fidelidad con conexión de salida XLR3 macho;
    \item 1 pie de micrófono con soporte acorde al micrófono disponible;
    \item 1  computadora con  sistema operativo  \emph{Windows} XP,  o  ulterior,  con placa  \emph{ethernet} en
    funcionamiento y \emph{AcqKnowledge 4.2} correctamente instalado;
    \item 1 cable \emph{ethernet},  cruzado de acuerdo a norma T568A, con un extremo dispuesto en el interior de
    la cámara anecoica y el otro en la sala de comandos.
\end{itemize}

%===================================================================================================================
%                                               NOCIONES PRELIMINARES
%===================================================================================================================
\section{Nociones preliminares}
El \biopac\ MP150 es un sistema modular para la adquisición de datos a altas tasas de muestreo que,  en conjunto con
los accesorios  correspondientes,  permite el registro simultáneo  de hasta  16 señales  simultáneamente.  Brinda la
posibilidad de configurar una  frecuencia de muestreo distinta por cada canal,  limitada  a $400\ \mathrm{kHz}$ como
máximo sumando la tasa de todos los canales.  Además,  su  comunicación con la computadora encargada del registro se
realiza a través de un cable \emph{ethernet}.  Como puede apreciarse en la Fig.  \ref{biopac},  el equipo incluye el
\emph{software} \emph{AcqKnowledge 4.2} con  el cual se comanda el proceso de  adquisición,  se visualizan de manera
\emph{on-line} los datos  entrantes y se analizan  de manera \emph{off-line} registros  ya adquiridos,  brindando la
posibilidad de aplicar sobre ellos herramientas de análisis y procesamiento, entre otras prestaciones.

\begin{figure}[!t]
  \centering
  \includegraphics[width=0.5 \textwidth]{mp150.jpg}
  \caption{Sistema \biopac:  Módulo principal MP150,  módulo interfaz universal  UIM100C y CD con \emph{software} de
  manejo \emph{AcqKnowledge 4.2}.}\label{biopac}
\end{figure}

Por  otro  lado,  el \laryngograph\  EGG-A100  es un  electroglotógrafo  apto  para  ser  empleado  en investigación
científica,  en laboratorio  y en la  práctica clínica.  Como su  nombre lo indica,  a  partir de este  equipo puede
obtenerse la señal  de EGG,  la cual brinda  información respecto a la  actividad de las cuerdas  vocales durante el
proceso de  producción del habla.  Más  precisamente,  la señal de  EGG refleja el  patrón de variación  del área de
contacto  existente  entre   las  cuerdas  vocales  durante   la  fonación.   Como  puede  observarse   en  la  Fig.
\ref{laryngograph},  en el  panel frontal se  cuenta con un  control continuo de  ganancia --en la  esquina superior
izquierda--  y la  conexión de  entrada para  el cable  paciente --en  la esquina  superior derecha--.  En  el panel
trasero,  cuenta con su salida analógica de $6\ V$ pico a pico de máxima excursión, la cual puede ser conectada a un
\emph{display} para  visualización;  o bien,  a un  conversor analógico-digital para  adquisición,  almacenamiento y
procesamiento digital.  En  el panel trasero,  se  encuentra el conector  correspondiente a su  alimentación por una
fuente de grado médico, AC/DC de $5\ V$.

\begin{figure}[!b]
  \centering
  \includegraphics[width=0.5 \textwidth]{laryngograph.png}
  \caption{\laryngograph\ EGG-A100: panel frontal y electrodos.}\label{laryngograph}
\end{figure}

%===================================================================================================================
%                                                   INSTRUCCIONES
%===================================================================================================================
\section{Instrucciones}
Las instrucciones que se listan  a continuación están basadas en la hipótesis de  que \emph{todo} el equipamiento de
adquisición --\biopac\  MP150,  \laryngograph\ EGG-A100  y accesorios--  se encuentra  en el  interior de  la cámara
anecoica, la computadora con la cual se hará el registro de las señales se encuentra en la sala de comandos y que la
comunicación se establecerá a través de un cable \emph{ethernet} instalado específicamente para este fin.
%-------------------------------------------------------------------------------------------------------------------
\subsection{Guía para la conexión del \emph{hardware} de adquisición}
\begin{enumerate}
  \item En el interior de la cámara,  acoplar el módulo  UIM100C sobre el lado derecho del módulo principal \biopac\
  MP150    (ver    ref.    \textcircled{\scriptsize    1}    y    \textcircled{\scriptsize    2}    de    la    Fig.
  \ref{MP150_UIM100C_DA100C}).
  \item Acoplar el módulo  DA100C sobre el lado derecho del módulo  UIM100C (ver ref.  \textcircled{\scriptsize 2} y
  \textcircled{\scriptsize 3} de la Fig.  \ref{MP150_UIM100C_DA100C}).
\end{enumerate}

\emph{\underline{Nota:} la  conexión del módulo UIM100C  con el MP150  siempre debe  realizarse acoplando  el módulo
UIM100C directamente sobre el lado derecho del MP150.}

\begin{figure}[!t]
\centering
  \includegraphics[width=0.6 \textwidth]{MP150_UIM100C_DA100C.pdf}\\
  \caption{Vista frontal del  sistema \biopac\,  luego del acoplamiento  de los módulos UIM100C y  DAC100C al módulo
  principal     MP150.     \emph{Referencias:}    \textcircled{\scriptsize     1}     Módulo     principal    MP150.
  \textcircled{\scriptsize 2} Módulo  UIM100C.  \textcircled{\scriptsize 3} Módulo DA100C.  \textcircled{\scriptsize
  4} Selector de canal.}\label{MP150_UIM100C_DA100C}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{2}
  \item En la  cara superior del módulo  DA100C,  colocar el selector de  canal en la \textbf{posición  1} (ver ref.
  \textcircled{\scriptsize 4} de la Fig.  \ref{MP150_UIM100C_DA100C}).  \label{CS_DA100C}
  \item En el panel frontal del módulo DA100C, seleccionar \textsf{1000} en el menú \textsf{GAIN}, \textsf{Off} para
  el menú  \textsf{10HzLP},  \textsf{5 kHz} para el  menú \textsf{LP} y  \textsf{0.05 Hz}  para el  menú \textsf{HP}
  (ver ref.  \textcircled{\scriptsize 3} de la Fig.  \ref{MP150_UIM100C_DA100C}).  \label{DA100C_Conf_item}
\end{enumerate}

\emph{\underline{Nota:} dado que  el  módulo}  \textsf{DA100C}  \emph{fue  modificado  a  pedido,  al fijar el menú}
\textsf{LP} \emph{en} \textsf{5  KHz},  \emph{se  fija  en  realidad  una  frecuencia  de  corte de} \textsf{25 KHz}
\emph{para el filtro pasa-bajos}.

\begin{enumerate}
  \setcounter{enumi}{4}
  \item  Conectar  el   módulo  adaptador  TCI115  al  módulo  DA100C   (ver  ref.   \textcircled{\scriptsize  5}  y
  \textcircled{\scriptsize 6} de la Fig.  \ref{TCI_DAC}).
\end{enumerate}

\begin{figure}[!t]
\centering
  \includegraphics[width=0.36 \textwidth]{TCI_DAC.pdf}
  \caption{Conexión del TCI115  al  módulo  DA100C.  \emph{Referencias:}  \textcircled{\scriptsize  5} Panel lateral
  derecho del adaptador TCI115.  \textcircled{\scriptsize 6} Panel frontal del módulo DA100C.} \label{TCI_DAC}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{5}
  \item En el panel trasero del MP150, dejar el interruptor de encendido en el estado de apagado, co\-lo\-can\-do el
  botón  \textsf{POWER}  en   la  posición  `\textsf{OFF}'  (ver  ref.   \textcircled{\scriptsize   7}  de  la  Fig.
  \ref{MP150_trasero}).
\end{enumerate}

\begin{figure}[!b]
\centering
  \includegraphics[width=0.25 \textwidth]{MP150_trasero_labels.pdf}\\
  \caption{Panel trasero del módulo principal MP150.  \emph{Referencias:} \textcircled{\scriptsize 7} Interruptor de
  encendido/apagado.  \textcircled{\scriptsize  8}  Conector  de  alimentación.  \textcircled{\scriptsize  9} Puerto
  \emph{ethernet} de comunicación con la computadora.}\label{MP150_trasero}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{6}
  \item Conectar la fuente de alimentación \textsf{AC150A-1} en la entrada \textsf{DC IN - 12 V/1 A} dispuesta en el
  panel trasero del MP150 (ver ref.  \textcircled{\scriptsize 8} de la Fig.  \ref{MP150_trasero}).
  \item Conectar el cable de red en el puerto etiquetado \textsf{ETHERNET}, dispuesto en la parte inferior del panel
  trasero del MP150 (ver ref.  \textcircled{\scriptsize 9} de la Fig.  \ref{MP150_trasero}).
  \item Acoplar el micrófono al conector XLR3 hembra dispuesto  en el extremo libre del cable del TCI115 y colocarlo
  luego en el soporte del pie de micrófono.
  \item Girar  la perilla  de ganancia,  etiquetada  como \textsf{Gain}  en el  panel frontal  del electroglotógrafo
  \laryngograph\   EGG-A100,   en   sentido   antihorario   hasta   alcanzar   el   extremo   izquierdo   (ver  ref.
  \textcircled{\scriptsize 10} de la Fig.  \ref{laryngo_front_labels}).
  \item Conectar  el cable paciente con  los electrodos en el  puerto \textsf{Electrodes},  en el  panel frontal del
  electroglotógrafo    \laryngograph\   EGG-A100    (ver   ref.    \textcircled{\scriptsize    11}   de    la   Fig.
  \ref{laryngo_front_labels}).   \emph{\underline{Nota:}  este   equipo  cuenta  con  tres   tamaños  diferentes  de
  electrodos y su elección dependerá de la contextura física del sujeto voluntario y,  en particular,  del tamaño de
  la laringe del mismo.  Para mayor información,  ver  el Apartado\#3,  situado en la pág.  \pageref{proto3} de este
  protocolo.}
  \item Conectar un extremo  del cable de transmisión de la  señal de EGG en el conector  RCA hembra etiquetado como
  \textsf{out},   situado   en  el   panel  trasero   del  electroglotógrafo   \laryngograph\  EGG-A100   (ver  ref.
  \textcircled{\scriptsize 12} de la Fig.  \ref{laryngograph_tras}).  \label{eggbiopac1}
\end{enumerate}

\begin{figure}[!t]
  \centering
  \includegraphics[width=0.4 \textwidth]{laryngo_front_labels.png}\\
  \caption{\laryngograph\  EGG-A100:  panel frontal.  \emph{Referencias:}  \textcircled{\scriptsize  10}  Control de
  ganancia.  \text{\textcircled{\scriptsize 11} Puerto de entrada de cable paciente.}}\label{laryngo_front_labels}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{12}
  \item Conectar el otro extremo del cable de transmisión de  la señal de EGG en la \textbf{entrada analógica 2} del
  módulo UIM100C,  marcada  con el número  \textsf{2} en la sección  \textsf{ANALOG CHANNELS} del  panel frontal del
  módulo  (ver ref.  \textcircled{\scriptsize  2} de  la Fig.  \ref{MP150_UIM100C_DA100C}).  \emph{\underline{Nota:}
  se requerirá  un cable con un  conector jack plug mono  de 3.5 mm en  un extremo --UIM100C-- y  conector RCA macho
  --EGG-A100-- en  el otro;  o  bien,  deberá hacer uso  del adaptador adecuado  dependiendo del  tipo de  cable que
  posea.} \label{eggbiopac2}
  \item Conectar la  fuente de alimentación en  el conector \textsf{+ 5  V/0.3 A},  situado en el  panel trasero del
  electroglotógrafo    \laryngograph\   EGG-A100    (ver   ref.    \textcircled{\scriptsize    13}   de    la   Fig.
  \ref{laryngograph_tras}).
  \item Girar  la perilla  de ganancia,  etiquetada  como \textsf{Gain}  en el  panel frontal  del electroglotógrafo
  \laryngograph\   EGG-A100,    en    sentido   horario    hasta   alcanzar   el    extremo   derecho    (ver   ref.
  \textcircled{\scriptsize 10} de la Fig.  \ref{laryngo_front_labels}).
  \item Encender el \biopac\ desde el interruptor \textsf{POWER},  dispuesto en el panel trasero del MP150 (ver ref.
  \textcircled{\scriptsize 7} de la Fig.  \ref{MP150_trasero}).
  \item  En  la sala  de  comandos,  conectar la  computadora  a la  red  de  alimentación  eléctrica  $220\  V/ 50\
  \mathrm{Hz}$.  Emplear una UPS, en caso de ser posible.
  \item Encender la computadora y ejecutar \emph{Windows} como sistema operativo.
  \item Conectar el cable \emph{ethernet} a la placa de red de la computadora.
  \item Verificar que el módulo MP150 esté encendido y correctamente conectado a la computadora.  \label{verif}
\end{enumerate}

\begin{figure}[!t]
\centering
  \includegraphics[width=0.38 \textwidth]{laryngograph_tras.png}\\
  \caption{\laryngograph\  EGG-A100:  panel trasero.  \emph{Referencias:}  \textcircled{\scriptsize 12}  Conector de
  salida de señal.  \textcircled{\scriptsize 13} Conector de alimentación.} \label{laryngograph_tras}
\end{figure}

%-------------------------------------------------------------------------------------------------------------------
\subsection{Guía para la configuración del \emph{software} de adquisición}
\begin{enumerate}
\setcounter{enumi}{20}
  \item Ejecutar el \emph{software} \emph{AcqKnowledge 4.2}.  En caso  que la comunicación entre el módulo principal
  MP150 y la  computadora falle,  aparecerá una ventana titulada  `\textsf{Choose MP150}' como la que  se muestra en
  la   \text{Fig.   \ref{ChooseMP150}}.   Realizar   nuevamente  el   paso   \ref{verif}   y   presionar   el  botón
  `\textsf{Refresh  Now}' hasta  poder seleccionar  la opción  \textsf{Work with:  1110A-0000FA3}.  No  habilitar en
  ningún caso la opción `\textsf{Do not ask again at startup}'.
%    \emph{Puede ejecutar el software sin poseer conectado el hardware seleccionando la opción} `\textsf{No MP150 Hardware}'. \emph{En este caso, sólo podrá visualizar o procesar registros ya obtenidos por lo cual no resulta de interés en el marco de este protocolo de adquisición.} \textbf{(verificar si da el mismo número con otra pc, por ejemplo la notebook de Gaston)}.
\end{enumerate}

\begin{figure}[!b]
\centering
  \includegraphics[width=0.45 \textwidth]{ChooseMP150.png}
  \caption{Ventana de  inicialización de  \emph{AcqKnowledge 4.2}  en caso de  falla de  comunicación con  el módulo
  MP150.} \label{ChooseMP150}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{21}
  \item Aparecerá luego otra  ventana titulada \textsf{AcqKnowledge} que le preguntará  `\textsf{What would you like
  to do?}',  seleccionar como  respuesta  \textsf{Create  and/or  Record  a  new  experiment}.  Debajo de la oración
  `\textsf{Click ``OK'' to perform the following:  }',  seleccionar \textsf{Create empty graph} y finalmente,  en la
  esquina inferior derecha, presionar \textsf{OK}.  \label{ventanainicio}
\end{enumerate}

\emph{\underline{Nota:} en  caso de poseer  almacenada una configuración  previamente establecida en  un archivo del
tipo ``*.gtl'', diríjase directamente al Apéndice A que se encuentra en la pág.  \pageref{selecttemplate}.}

\begin{enumerate}
\setcounter{enumi}{22}
  \item  Para  configurar   los   canales   a   emplear   durante   la  adquisición,   seleccionar  `\textsf{Set  Up
  \text{Channels...}}'  en el  menú \textsf{MP150},  ubicado  en la  parte superior  de la  ventana principal  de la
  aplicación.
\end{enumerate}

\begin{figure}[!t]
\centering
  \includegraphics[width=0.75 \textwidth]{SetUpChannels.png}
  \caption{Ventana  de  configuración  de  canales  de  adquisición  y  monitoreo  del  sistema  \biopac\,  mediante
  \emph{AcqKnowledge 4.2}.} \label{SetUpChannels}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{23}
  \item Seleccionar la pestaña `\textsf{Analog}' y,  sólo  si la opción está disponible,  presionar `\textsf{View by
  \text{Modules...}}'.  Tras  la  realización  de  este  conjunto  de  acciones  deberá  ver  una  ventana  titulada
  `\textsf{Input channels setup for `MP150 000FA3'}' como la que se muestra en la Fig.  \ref{SetUpChannels}.
  \item En la parte  inferior izquierda de la ventana mostrada en  la Fig.  \ref{SetUpChannels},  presionar el botón
  `\textsf{Add New  Module...}'.  Esta acción abrirá  una nueva ventana  que le preguntará  qué módulo desea  o debe
  agregar, `\textsf{What type of module should be added?}'.
  \item Buscar el módulo \textsf{DA100C} en la lista, seleccionarlo y presionar `\textsf{Add}'.  Aparecerá una nueva
  ventana titulada `\textsf{Choose  Channel Switch Position}',  que le  preguntará cuál es la  posición del selector
  de  canal que  se encuentra  en la  cara superior  del  módulo  físico  DA100C.  Seleccionar  el  \textbf{canal 1}
  mediante el deslizador horizontal mostrado en la Fig.  \ref{ChannelSwitch}.  \label{CS_DA100C_soft}
\end{enumerate}

\begin{figure}[!b]
\centering
  \includegraphics[width=0.55 \textwidth]{ChannelSwitch.png}
  \caption{Ventana de selección de posición del módulo DA100C.} \label{ChannelSwitch}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{26}
  \item Verificar que el  número  de  canal  seleccionado  en  el  paso  \ref{CS_DA100C_soft} coincida con el fijado
  previamente  por  \emph{hardware},   de   acuerdo  al  paso  \ref{CS_DA100C}  del   presente  apartado  (ver  ref.
  \textcircled{\scriptsize 4} de la Fig.  \ref{MP150_UIM100C_DA100C}).  Finalmente, presionar `\textsf{OK}'.
  \item   Sobre   la  ventana   denominada   `\textsf{DA100C  Configuration}'   como   la   mostrada   en   la  Fig.
  \ref{DA100C_Config},   seleccionar   \textsf{1000}  en   el  menú   \textsf{GAIN},   \textsf{Off}  para   el  menú
  \textsf{10HzLP},  \textsf{5  kHz}  para  el  menú \textsf{LP}  y  \textsf{0.05  Hz}  para  el  menú  \textsf{HP} y
  \textsf{TCI - Custom} para el menú `\textsf{Connected to: }'.  \label{DA100C_Conf_item_soft}
\end{enumerate}

\begin{figure}[!t]
\centering
  \includegraphics[width=0.5 \textwidth]{DA100C_Config.png}
  \caption{Ventana de configuración de los parámetros de adquisición del módulo DA100C.} \label{DA100C_Config}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{28}
  \item Verificar que la configuración del  \emph{software} realizada de acuerdo al paso \ref{DA100C_Conf_item_soft}
  sea consistente no sólo con la que se  muestra en la Fig.  \ref{DA100C_Config},  sino también con la configuración
  del  \emph{hardware} realizada  de acuerdo  al  paso  \ref{DA100C_Conf_item}  del  presente apartado.  Finalmente,
  presionar `\textsf{OK}'.
  \item Sobre  la ventana  denominada `\textsf{Calibration}',  introducir  los valores  que se  muestran en  la Fig.
  \ref{DA100C_Calibration} y  presionar `\textsf{OK}'.  Al completar este  paso ha  finalizado la  configuración del
  canal de adquisición de la señal de voz proveniente del micrófono de adquisición.  \label{finvoz}
\end{enumerate}

\begin{figure}[!b]
\centering
  \includegraphics[width=0.46 \textwidth]{DA100C_Calibration.png}
  \caption{Ventana de calibración del sensor empleado con el módulo DA100C.} \label{DA100C_Calibration}
\end{figure}

Luego de finalizar el paso \ref{finvoz}, deberá ver una ventana como la que se muestra en la Fig.  \ref{finvoz_fig}.
Mediante la modificación del  campo `\textsf{Label}' de la fila correspondiente a  \textsf{DA100C} podrá colocar una
etiqueta a dicho canal, por ejemplo ``Voz''.  El uso de etiquetas es muy recomendado en esta tarea para poder llevar
a cabo una mejor identificación de los canales y,  de esta manera,  un trabajo más claro y ordenado.  Sobre el mismo
renglón,  dispone de información acerca del transductor y  del canal utilizados,  como así también de los siguientes
elementos para realizar modificaciones sobre la configuración establecida para este canal:

\begin{itemize}
  \item el  botón \includegraphics[height=0.015 \textheight]{llave.jpg} inicia  la configuración  y la  secuencia de
  calibración para el módulo de la fila o renglón correspondiente.
  \item  el  \emph{checkbox} \includegraphics[height=0.015  \textheight]{check.jpg}  ejecutará  el  procedimiento de
  calibración del  módulo al  comienzo de  la adquisición  cuando se  encuentre habilitado.  En  este protocolo,  se
  utiliza deshabilitado.
  \item  el botón  \includegraphics[height=0.015 \textheight]{remove.jpg}  elimina la  entrada de  configuración del
  módulo realizada.
\end{itemize}

\begin{enumerate}
\setcounter{enumi}{30}
  \item En la parte inferior izquierda de la  ventana `\textsf{Input channels setup for `MP150 000FA3'}' mostrada en
  la  Fig.  \ref{finvoz_fig},  presionar  el  botón `\textsf{Add  New  Module...}'.  Esta  acción  abrirá  una nueva
  ventana que,  nuevamente,  le preguntará qué módulo desea o  debe agregar,  `\textsf{What type of module should be
  added?}'.
\end{enumerate}

\begin{figure}[!t]
\centering
  \includegraphics[width=0.7 \textwidth]{finvoz_fig.png}
  \caption{Ventana de  configuración de los canales  de adquisición luego  de la  configuración del  módulo DA100C.}
  \label{finvoz_fig}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{31}
\item Buscar  en la  lista,  seleccionar \textsf{UIM100C  - A2}  y luego,  presionar  `\textsf{Add}'.  Aparecerá una
ventana  titulada `\textsf{UIM100C  Configuration}',  como la  mostrada  en  la  Fig.  \ref{UIM100C_Config},  que le
informará cuál es  la entrada analógica del módulo  UIM100C que seleccionó para configurar  --en este caso,  debería
ver \textsf{UIM100C Analog  Input:  2}-- y le permitirá  seleccionar el tipo de  transductor a emplear.  Seleccionar
`\textsf{Transducer: Custom}' y presionar `\textsf{OK}'.
\item Verificar  que la configuración  del \emph{software} que  acaba de realizar  coincida con  la realizada  en el
\emph{hardware} correspondiente,  de acuerdo a los pasos  \ref{eggbiopac1} y \ref{eggbiopac2} del presente apartado.
Esto  implica  que  debe  corroborar que  la  salida  del  electroglotógrafo  \laryngograph\  EGG-A100  se encuentre
efectivamente conectada a la entrada analógica 2 del módulo UIM100C de \biopac.
\end{enumerate}

\begin{figure}[!t]
\centering
  \includegraphics[width=0.5 \textwidth]{UIM100C_Config.png}\\
  \caption{Ventana de configuración de la adquisición mediante el módulo UIM100C.} \label{UIM100C_Config}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{33}
  \item  Sobre   la  ventana  `\textsf{Calibration}',   introducir   los  valores  que   se  muestran  en   la  Fig.
  \ref{UIM100C_Calibration} y  presionar `\textsf{OK}'.  Al completar este  paso ha finalizado  la configuración del
  canal de adquisición de la señal de EGG proveniente desde el \laryngograph\ EGG-A100.  \label{finegg}
\end{enumerate}

\begin{figure}[!b]
\centering
  \includegraphics[width=0.5 \textwidth]{UIM100C_Calibration.png}
  \caption{Ventana de calibración del sensor empleado con el módulo UIM100C.} \label{UIM100C_Calibration}
\end{figure}

Luego  de  finalizar  el  paso  \ref{finegg},   deberá  ver  una   ventana  como  la  que  se  muestra  en  la  Fig.
\ref{finvozegg_fig}.   Mediante  la   modificación  del  campo   `\textsf{Label}'  de  la   fila  correspondiente  a
\textsf{UIM100 -  A2} podrá colocar una  etiqueta a dicho canal,  por  ejemplo ``EGG''.  El uso de  etiquetas es muy
recomendado en esta tarea para  poder llevar a cabo una mejor identificación de  los canales y,  de esta manera,  un
trabajo más claro  y ordenado.  Sobre el mismo renglón,  dispone  de información acerca del transductor  y del canal
utilizados,  como así también  de  los  siguientes  elementos  para  realizar  modificaciones sobre la configuración
establecida para este canal:

\begin{itemize}
  \item el  botón \includegraphics[height=0.015 \textheight]{llave.jpg} inicia  la configuración  y la  secuencia de
  calibración para el módulo de la fila o renglón correspondiente.
  \item  el  \emph{checkbox} \includegraphics[height=0.015  \textheight]{check.jpg}  ejecutará  el  procedimiento de
  calibración del  módulo al  comienzo de  la adquisición  cuando se  encuentre habilitado.  En  este protocolo,  se
  utiliza deshabilitado.
  \item  el botón  \includegraphics[height=0.015 \textheight]{remove.jpg}  elimina la  entrada de  configuración del
  módulo realizada.
\end{itemize}

\emph{\underline{Nota:} está en  condiciones  de  almacenar  la  configuración  que  acaba de finalizar.  Para ello,
diríjase al Apéndice A situado en la pág.  \pageref{selecttemplate} de este documento.}

\begin{figure}[!t]
\centering
  \includegraphics[width=0.85 \textwidth]{finegg_fig.png}
  \caption{Ventana de configuración de los canales de entrada al finalizar la configuración.} \label{finvozegg_fig}
\end{figure}

\begin{enumerate}
  \setcounter{enumi}{34}
  \item Una vez  que se han establecido  los parámetros de adquisición  asociados a cada canal,  el  próximo paso es
  especificar la  configuración del proceso de  adquisición propiamente dicho respecto  a cómo debe  ser adquirida y
  almacenada la información,  la  tasa o frecuencia de muestreo a  la cual se realizará y la  duración que tendrá el
  proceso.  Para ello,  seleccionar  `\textsf{Set Up  Acquisition}' en el  menú \textsf{MP150}  ubicado en  la parte
  superior de la ventana principal, el cual abrirá una ventana como la que se observa en la Fig.  \ref{acq_setup}.
\end{enumerate}

\begin{figure}[!t]
\centering
  \includegraphics[width=0.85 \textwidth]{Acq_setup.png}
  \caption{Ventana de configuración del proceso de adquisición.} \label{acq_setup}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{35}
  \item Con respecto al almacenamiento transitorio de la información durante el proceso de adquisición,  seleccionar
  la opción \textsf{Record and Save Once using Memory}.
  \item Fijar una frecuencia de muestreo de 50000 Hz, \textsf{Sample rate: 50000 samples/second}.
\end{enumerate}

La duración del proceso de adquisición --\textsf{Acquisition Length}--  es un parámetro que no puede establecerse en
esta instancia  dado que dependerá  del tipo de emisión  que pretenda registrarse.  Para  mayor información,  ver la
etapa de \emph{adquisición} del  apartado  ``Instrucciones  para  el  entrenamiento  y  adquisición de las emisiones
vocales a solicitar'', situado en la pág.  \pageref{proto4} del presente protocolo.

%===================================================================================================================
%                                                    APÉNDICE A
%===================================================================================================================
\newpage
\renewcommand\thefigure{2.A.\arabic{figure}}
\setcounter{figure}{0}
%\renewcommand\thesection{A.\arabic{section}}
%\setcounter{section}{0}
\section*{Apéndice 2.A: Uso de plantillas}
\addcontentsline{toc}{section}{Apéndice 2.A: Uso de plantillas}
\label{selecttemplate}
\noindent
A los fines de ahorrar tiempo y trabajo, como así también reducir la posibilidad de errores, es recomendable guardar
una plantilla o \emph{template} con la configuración que  acaba de realizar.  Para ello,  en la parte inferior de la
ventana de  configuración de los canales  de entrada --mostrada  en la  Fig.  \ref{finvozegg_fig}-- pulsar  el botón
`\textsf{Save as Graph Template...}'.  Se  abrirá una ventana que le solicitará la ubicación  en el disco rígido del
archivo que  contendrá la  información establecida  hasta aquí.  Guardarlo donde  crea conveniente  y con  un nombre
apropiado,   por  ejemplo  ``Config\_Voz\_EGG.gtl''.   Podrá  hacer  uso  del  mismo  la  próxima  vez  que  ejecute
\emph{AcqKnowledge 4.2} seleccionando en  la ventana de inicio las opciones `\textsf{Use  recent graph template:}' u
`\textsf{Open graph template from disk...}' --las cuales se encuentran debajo de la oración `\textsf{Click ``OK'' to
perform the following:}'--.

\begin{figure}[!b]
\centering
  \includegraphics[width=0.70 \textwidth]{apendice_a.png}
  \caption{Ventana de selección de plantilla previamente almacenada.} \label{plantilla}
\end{figure}

Si el archivo que usted generó se encuentra en la lista que se muestra debajo,  marcar la opción `\textsf{Use recent
graph template:}',  seleccionar en la lista  el archivo deseado y,  finalmente,  presionar \textsf{OK}.  Si,  por el
contrario,  su configuración \emph{no}  se  encuentra  en  la  lista  que  se  muestra debajo,  seleccione la opción
`\textsf{Open graph template  from  disk...}'  y  luego  presionar  \textsf{OK}.  Ante  ello,  se abrirá una ventana
titulada `\textsf{Please choose a graph template}' que le solicitará la búsqueda en el disco y,  posteriormente,  la
selección del archivo que contiene la configuración que desea.
%===================================================================================================================
%===================================================================================================================
%===================================================================================================================
% 'Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>'
% 2020-02-14
%===================================================================================================================
%z===================================================================================================================
