%===================================================================================================================
%===================================================================================================================
%                                 PROTOCOLO PARA LA ADQUISICIÓN Y EL ALMACENAMIENTO
%                                DE SEÑALES BIOMÉDICAS ASOCIADAS A LA ACTIVIDAD VOCAL
%-------------------------------------------------------------------------------------------------------------------
%           CAPÍTULO DOS: INSTRUCCIONES PARA LA CONEXIÓN DEL EQUIPAMIENTO DE COMUNICACIÓN SUJETO-OPERADOR
%-------------------------------------------------------------------------------------------------------------------
%...................................................................................................................
% Autores:
%   Ariel Stassi <aestassi@ingenieria.uner.edu.ar>
%   Gabriel Alzamendi <galzamendi@ingenieria.uner.edu.ar>
%   Juan Manuel Miramont <jmiramont@ingenieria.uner.edu.ar>
%   Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>
% 2020-02-14
%===================================================================================================================
%===================================================================================================================
\pagestyle{main}
\chapter{\label{proto1}Instrucciones para la conexión del equipamiento de comunicación sujeto-operador}
%===================================================================================================================
%                                                     OBJETIVOS
%===================================================================================================================
\section{Objetivos}
Los objetivos principales de este apartado son:
\begin{itemize}
  \item Realizar  una lista  de los materiales  necesarios para el  establecimiento de  una comunicación  correcta y
  fluida entre un operador en la sala de comandos y un voluntario en el interior de la cámara anecoica.
  \item Proveer un método  secuencial  para  el  montaje  y  ajuste  de  los  elementos que permitan la comunicación
  sujeto-operador antedicha.
\end{itemize}
%===================================================================================================================
%                                                      ALCANCE
%===================================================================================================================
\section{Alcance}
El presente  documento ha sido  desarrollado para su utilización  en las instalaciones  de la cámara  anecoica de la
\fiuner\ con  elementos provistos por  el \lc\ y el  \lsydnl,  ambos pertenecientes a  dicha institución.  La cámara
anecoica  de la  \fiuner\ cuenta  con dos  recintos:  la cámara  anecoica propiamente  dicha y  la sala  de comandos
asociada.  En este  contexto,  se ha diseñado  este protocolo para el  establecimiento de la  comunicación entre dos
personas,  ubicadas  cada una  en un  recinto distinto,  haciendo  el  uso  adecuado  de  los  recursos tecnológicos
disponibles.
%===================================================================================================================
%                                               MATERIALES NECESARIOS
%===================================================================================================================
\section{Materiales necesarios}
\begin{itemize}
    \item 1 micrófono dinámico, preferentemente de alta fidelidad, con conexión de salida XLR3 macho;
    \item 1 pie de micrófono con soporte acorde al micrófono disponible;
    \item 1 juego de auriculares de alta fidelidad;
    \item 1  computadora con sistema  operativo \emph{Windows} XP  o ulterior,  con placa  de sonido con  entrada de
    micrófono tipo \emph{jack}  hembra  \emph{estéreo}  de  $3.5\  mm$  y  salida  de  audio tipo \emph{jack} hembra
    \emph{estéreo} de $3.5\ mm$;
    \item 1 consola \textsf{MACKIE} 1402-VLZ PRO;
    \item 1 cable de audio \emph{estéreo} para auriculares, con \emph{jack} hembra \emph{estéreo} de $3.5\ mm$ en el
    interior de la cámara y dos conectores RCA macho en la sala de comandos;
    \item 1 adaptador de dos conectores RCA macho a un solo conector RCA macho;
    \item 1 adaptador de conector RCA macho a \emph{jack plug} TS de $\frac{1}{4}\ pulg \approx 6.35\ mm$;
    \item 1 cable para micrófono,  con conector XLR3 hembra en  el interior de la cámara y \emph{jack plug} TS macho
    de $\frac{1}{4}\ pulg \approx 6.35\ mm$ en la sala de comandos;
    \item 1 cable de audio \emph{mono},  con conectores \emph{jack  plug} TS de $\frac{1}{4}\ pulg \approx 6.35\ mm$
    en un extremo,  y de $3.5\ mm$ en el otro;  o bien, 1 cable de audio \emph{mono} con conectores \emph{jack plug}
    TS  de $\frac{1}{4}\  pulg \approx  6.35\ mm$  en los  dos extremos  y un  adaptador de  \emph{jack plug}  TS de
    \text{$\frac{1}{4}\ pulg \approx 6.35\ mm$} a \emph{jack plug} TS de $3.5\ mm$;
    \item 1 juego de auriculares con micrófono incluido;
    \item 1 adaptador de  \emph{jack  plug}  TRS  --o  \emph{estéreo}--  de  $3.5\  mm$  a  \emph{jack plug} TRS --o
    \emph{estéreo}-- de $\frac{1}{4}\ pulg \approx 6.35\ mm$.
\end{itemize}
%===================================================================================================================
%                                               NOCIONES PRELIMINARES
%===================================================================================================================
\section{Nociones preliminares}
En la Fig.  \ref{consola_frente} puede observarse el panel frontal de la consola a utilizar, junto con sus elementos
correspondientes:
\begin{itemize}
  \item los \emph{parámetros de control} asociados a cada uno de los \emph{canales de entrada}, dispuestos a modo de
  columnas, en la parte inferior izquierda (para mayor información, ver Fig.  \ref{canal});
  \item la \emph{sección de conexiones de entrada}, en la parte superior izquierda (para mayor información, ver Fig.
  \ref{entrada});
  \item la \emph{sección de conexiones de salida},  en  la parte superior derecha (para mayor información,  ver Fig.
  \ref{salida}).
\end{itemize}

\begin{figure}[!t]
\centering
\includegraphics[width=0.7\textwidth]{Consola_frente.pdf}
\caption{Panel frontal de la consola  \textsf{MACKIE} 1402-VLZ PRO.  \emph{Referencias:} \textcircled{\scriptsize 1}
Control    de     ganancia    de    pre-mezcla.     \textcircled{\scriptsize     2}    Panel    \textsf{C-R/SOURCE}.
\textcircled{\scriptsize 3} Salida de auriculares.}\label{consola_frente}
\end{figure}

Esta consola,  o bandeja de mezcla, cuenta con seis entradas \emph{mono} --numeradas,  de izquierda a derecha, desde
\textsf{1} a \textsf{6}-- y cuatro entradas  \emph{estéreo} --numeradas,  de izquierda a derecha,  de \textsf{7-8} a
\textsf{13-14}--.   Su  disposición  se  corresponde  de  acuerdo  al   número  asociado  a  cada  canal  (ver  ref.
\textcircled{\scriptsize 5} de  la  Fig.  \ref{canal}).  En  la  Fig.  \ref{canal}  pueden  observarse los controles
asociados a cada canal con mayor detalle.

\begin{figure}[!t]
  \centering
  \includegraphics[height=0.45\textheight]{canal.pdf}
  \caption{Disposición de los parámetros  de control sobre cada una de  los canales de entrada.  \emph{Referencias:}
  \textcircled{\scriptsize 4} Control  de ganancia o \emph{fader}.  \textcircled{\scriptsize 5}  Indicador de número
  de  canal.   \textcircled{\scriptsize  6}  Interruptor  de  silenciado.  \textcircled{\scriptsize  7}  Control  de
  ecualización de componentes  en  frecuencias  agudas,  medias  y  graves.  \textcircled{\scriptsize  8} Control de
  \emph{panning}.  \textcircled{\scriptsize  9}  Control  de  los  envíos  del  canal.  \textcircled{\scriptsize 10}
  Interruptor de asignación de canal.} \label{canal}
\end{figure}

En la Fig.  \ref{consola_trasero} puede observarse el panel  trasero de la consola \textsf{MACKIE} 1402-VLZ PRO.  En
la parte superior izquierda se encuentran, de izquierda a derecha:  la toma de potencia y porta-fusible, interruptor
de encendido --el  cual enciende la consola cuando  se encuentra en la posición  \textsf{POWER ON},  indicada con el
símbolo \textsf{I}-- e interruptor de fuente de alimentación  fantasma o fuente \emph{phantom} --el cual enciende la
alimentación \emph{phantom} cuando se encuentra en la posición \textsf{PHANTOM ON}--.

\begin{figure}[!b]
  \centering
  \includegraphics[width=0.9\textwidth]{Consola_trasero.pdf}
  \caption{Panel trasero de la consola \textsf{MACKIE} 1402-VLZ PRO.}\label{consola_trasero}
\end{figure}

A su vez en marca de agua, en la parte inferior derecha de la Fig.  \ref{consola_trasero}, se encuentran un conjunto
de conexiones de salida e inserciones, las cuales no serán empleadas a lo largo de este documento.

\begin{center}
\text{\emph{\underline{Nota:} el fusible  de  protección  de  la  consola}  \textsf{MACKIE}  1402-VLZ PRO \emph{debe
cumplir con:}}\\ \emph{\textbf{corriente nominal de $\mathbf{250\  mA}$,  tensión nominal $\mathbf{250\ V}$ y ser de
fusión lenta.}}
\end{center}
%===================================================================================================================
%                                                   INSTRUCCIONES
%===================================================================================================================
\section{Instrucciones}
\begin{enumerate}
  \item En el interior de la cámara,  acoplar el micrófono de monitoreo al conector XLR3 hembra y colocarlo luego en
  el soporte del pie de micrófono.  \label{pasoa}
  \item Conectar los auriculares de alta fidelidad al \emph{jack} hembra \emph{estéreo} de $3.5\ mm$ dispuesto en el
  interior de la cámara.
  \item En la sala de  comandos,  conectar la consola a la red de alimentación  eléctrica $220\ V/ 50\ \mathrm{Hz}$,
  mediante un  transformador de corriente  alterna de $220\  V-110\ V$.  Emplear una  UPS,  en caso de  ser posible.
  Verificar que  el interruptor del transformador  involucrado en la alimentación  de la consola se  encuentre en la
  posición encendido, indicada como \textsf{I}.
  \item Conectar el cable de  alimentación desde la salida del transformador a la toma  de potencia que se encuentra
  en la parte superior izquierda del panel trasero de la consola (ver Fig.  \ref{consola_trasero}).
  \item  Encender  la  consola  mediante  el  interruptor  de  encendido  ubicado  en  su  panel  trasero  (ver Fig.
  \ref{consola_trasero}).  \emph{\underline{Precaución:} no encender la fuente de alimentación phantom.}
  \item  De   manera  previa   a  cualquier  maniobra   de  conexión   o  desconexión,   colocar   \emph{todos}  los
  \text{\emph{faders}}\footnote{Término empleado en la  ingeniería del sonido para hacer referencia  a un elemento o
  dispositivo que,  mediante un  desplazamiento generalmente \emph{lineal},  modifica de manera  gradual el nivel de
  una señal  de audio.}  de la  consola en su  extremo inferior,  indicado  mediante el  símbolo $\infty$  (ver ref.
  \textcircled{\scriptsize   4}  de   la   Fig.   \ref{canal}).   \underline{\emph{Aclaración:}}   \emph{todos}  los
  \emph{faders} incluye los  de  cada  \emph{canal},  enumerados  de  \textsf{1}  a  \textsf{6}  y de \textsf{7-8} a
  \textsf{13-14}  --ver ref.  \textcircled{\scriptsize  5} de  la Fig.  \ref{canal}--,  el  principal --\textsf{MAIN
  MIX}-- y el  de  pre-mezcla  --\textsf{CTL  ROOM/SUBMIX}--,  ubicados  en  el  extremo  inferior derecho del panel
  frontal (ver ref.  \textcircled{\scriptsize 1} de la Fig.  \ref{consola_frente}).
  \item Sobre los \textbf{canales 1} y \textbf{2}, colocar el botón \textsf{MUTE/ALT 3-4} en posición de liberado o
  \item Sobre los \textbf{canales 1} y \textbf{2},  colocar  en posición de ganancia unitaria --indicada mediante la
  letra  \textsf{U}--  las   perillas   \textsf{EQ}   --\textsf{HI},   \textsf{MID}   y   \textsf{LOW}--  (ver  ref.
  \textcircled{\scriptsize 7} de la Fig.  \ref{canal}).
  \item Sobre  los \textbf{canales 1} y  \textbf{2},  colocar la perilla \textsf{PAN}  en posición neutra,  indicada
  mediante el  símbolo $\blacktriangleleft  \blacktriangleright$ (ver ref.  \textcircled{\scriptsize  8} de  la Fig.
  \ref{canal}).
  \item Sobre los \textbf{canales 1} y \textbf{2} colocar los controles de envío \textsf{AUX} --\textsf{1 MON/EFX} y
  \textsf{2   EFX}--   en   su   extremo   izquierdo,    indicado   mediante   el   símbolo   $\infty$   (ver   ref.
  \textcircled{\scriptsize  9}  de  la  Fig.   \ref{canal}).   Mediante  el  ajuste  de  \textsf{AUX  1  MON/EFX}  y
  \textsf{AUX 2 EFX}  puede controlarse el nivel de señal  a enviar a las salidas \textsf{AUX  SEND 1} y \textsf{AUX
  SEND 2},  respectivamente,  dispuestas en la sección de salida de la consola \text{(para más información, ver ref.
  \textcircled{\scriptsize 14} y \textcircled{\scriptsize 15} de la Fig.  \ref{salida})}.  \label{pasob}
\end{enumerate}

Al finalizar el paso \ref{pasob}, verificar que la posición de cada uno de los controles asociados a los canales 1 y
2 se corresponda con la Fig.  \ref{canal}.

\begin{enumerate}
  \setcounter{enumi}{10}
  \item En el  sector \textsf{C-R/SOURCE} , deshabilitar \emph{todos}  los botones --\textsf{MAIN MIX},  \textsf{ALT
  3-4},  \textsf{TAPE} y \textsf{ASSIGN TO  MAIN MIX}-- colocándolos en la posición de  liberado o ``no presionado''
  (ver  ref.  \textcircled{\scriptsize 2}  de la  Fig.  \ref{consola_frente}).  De  esta  manera,  se  controlará el
  nivel  de  la   mezcla   de   las   señales   mediante   el   \emph{fader}   \textsf{CTL  ROOM/SUBMIX}  (ver  ref.
  \textcircled{\scriptsize 1}  de la Fig.  \ref{consola_frente}).  En  este modo de  operación,  sólo formarán parte
  de la mezcla  aquellos canales para los cuales  el interruptor \textsf{SOLO} se encuentre  presionado o habilitado
  (ver ref.  \textcircled{\scriptsize 10} de la Fig.  \ref{canal}).
  \item Conectar  el \emph{jack plug}  TS de $\frac{1}{4}\  pulg \approx 6.35\  mm$ correspondiente al  micrófono de
  monitoreo en la entrada  de línea del \textbf{canal 1} de la consola  (ver ref.  \textcircled{\scriptsize 5} de la
  Fig.  \ref{canal} y \textcircled{\scriptsize 11} de la Fig.  \ref{entrada}).
  \item Sobre  el \textbf{canal 1},  habilitar el  filtro pasa-altas,  presionando  el interruptor  \textsf{LOW CUT,
  \text{75 Hz, 18 dB/OCT}} (ver ref.  \textcircled{\scriptsize 12} de la Fig.  \ref{entrada}).
  \item Sobre  el \textbf{canal 1},  colocar  la perilla \textsf{TRIM}  en posición de  ganancia unitaria,  indicada
  mediante la letra \textsf{U} (ver ref.  \textcircled{\scriptsize 13} de la Fig.  \ref{entrada}).
  \item Sobre el \textbf{canal 1}, presionar el interruptor \textsf{SOLO} (ver ref.  \textcircled{\scriptsize 10} de
  la Fig.  \ref{canal}).
\end{enumerate}

\begin{figure}[t!]
  \centering
  \includegraphics[width=\textwidth]{Entradas.pdf}
  \caption{Sector  de  conexiones   de   entrada   y   controles   asociados   a  cada  canal.   \emph{Referencias:}
  \text{\textcircled{\scriptsize 11}  Entrada de línea.}  \textcircled{\scriptsize 12} Estado  de filtro pasa-altas.
  \textcircled{\scriptsize 13} Ajuste de ganancia de entrada.} \label{entrada}
\end{figure}

\begin{enumerate}
  \setcounter{enumi}{15}
  \item Conectar el micrófono del operador,  el cual se  encuentra montado sobre la vincha de los auriculares,  a la
  entrada de micrófono de la placa de sonido de la computadora.
  \item Conectar un cable  de audio \emph{mono} desde la salida  de audio de la placa de sonido  hacia la entrada de
  línea del \textbf{canal  2} de la consola  (\text{ver ref.  \textcircled{\scriptsize 5} de  la Fig.  \ref{canal} y
  \textcircled{\scriptsize 11}  de la Fig.  \ref{entrada}).}  \emph{\underline{Nota:}} para esta  maniobra requerirá
  de un cable de audio  \emph{mono}  con  un  \emph{jack  plug}  TS  de  $\frac{1}{4}\  pulg  \approx 6.35\ mm$ y un
  \emph{jack plug} TS de $3.5\ mm$;  o bien,  de un cable con dos \emph{jack plugs} TS de $\frac{1}{4}\ pulg \approx
  6.35\ mm$ y un adaptador  de \emph{jack plug} TS de $\frac{1}{4}\ pulg \approx 6.35\  mm$ a \emph{jack plug} TS de
  $3.5\ mm$.
  \item Sobre el \textbf{canal 2}, libere el interruptor \textsf{SOLO} (ver ref.  \textcircled{\scriptsize 10} de la
  Fig.  \ref{canal}).
  \item  Desde el  \emph{Panel de  control} de  \emph{Windows},  deshabilitar \emph{todos}  los sonidos  propios del
  sistema operativo.  Para mayor detalle,  puede dirigirse al Apéndice  A del presente apartado,  ubicado en la pág.
  \pageref{ap1a}.
  \item Desde la  configuración de los \emph{dispositivos  de grabación} de \emph{Windows},  habilitar  la opción de
  enviar a la  salida de la placa de sonido  la señal adquirida por el micrófono  del operador.  Para mayor detalle,
  puede dirigirse al Apéndice B del presente apartado, ubicado en la pág.  \pageref{ap1b}.
\end{enumerate}

En la  Fig.  \ref{salida} puede  observarse la disposición  de las conexiones  de salida  en el  panel frontal  y en
particular, de \textsf{AUX SEND 1}, envío del cual se hace uso en este protocolo.
\begin{figure}[!t]
  \centering
  \includegraphics[width=\textwidth]{Salidas.pdf}
  \caption{\text{Sector de conexiones  de salida y salida  de interés.  \emph{Referencias:} \textcircled{\scriptsize
  14} \textsf{AUX  SEND 1},}  envío de  mezcla de canales  ponderados por  la perilla  \textsf{AUX1} de  cada canal.
  \textcircled{\scriptsize  15}  \textsf{AUX  SEND  2},  envío  de  mezcla  de  canales  ponderados  por  la perilla
  \textsf{AUX2} de cada canal.} \label{salida}
\end{figure}

\begin{enumerate}
  \setcounter{enumi}{20}
  \item En la sección de  salida de la consola,  conectar a la salida \textsf{AUX SEND 1}  de la consola el cable de
  audio estéreo  que transmitirá la señal  a enviar a los auriculares  del sujeto en  el interior de  la cámara (ver
  ref.  \textcircled{\scriptsize 14}  de la  Fig.  \ref{salida}).  \emph{\underline{Nota:}} para  esta maniobra,  se
  propone hacer uso de dos adaptaciones:  en primer lugar,  adaptar  los dos conectores RCA macho a sólo un conector
  RCA macho y en segundo lugar, adaptar el conector RCA macho a un \emph{jack plug} TS de $6.5\ mm$.
  \item  Ajustar  el  nivel  de  la  señal  enviada  del  operador  al  sujeto  mediante  el  control  \textsf{AUX1}
  correspondiente   al   \textbf{canal    2}   de   la   consola   (ver    ref.    \textcircled{\scriptsize   5}   y
  \textcircled{\scriptsize 9} de la Fig.  \ref{canal}).
  \item Conectar  los auriculares del  operador a la  salida \textsf{PHONES} localizada  en el  panel frontal  de la
  consola (ver  ref.  \textcircled{\scriptsize 3} de  la Fig.  \ref{consola_frente}).  \emph{\underline{Nota:}} para
  esta maniobra  requerirá de un adaptador  de \emph{jack plug} TRS  --o \emph{estéreo}-- de $3.5\  mm$ a \emph{jack
  plug} TRS --o \emph{estéreo}-- de $\frac{1}{4}\ pulg \approx 6.35\ mm$.
  \item Se sugiere colocar  los \emph{faders} correspondientes a los \textbf{canales 1}  y \textbf{2} en la posición
  de ganancia  unitaria,  indicada mediante  la letra  \textsf{U},  aunque el  valor de  ganancia puede  ajustarse a
  conveniencia si se  requiere  (ver  ref.  \textcircled{\scriptsize  4}  y  \textcircled{\scriptsize  5} de la Fig.
  \ref{canal}).
  \item Realizar verificación de la comunicación y  ajustes de ganancia necesarios.  Para ello,  tener en cuenta que
  el \textbf{nivel de  la señal enviada del  operador al sujeto} se  controla mediante el \emph{fader}  y la perilla
  \textsf{AUX1} asociadas  al \textbf{canal  2} y  que el \textbf{nivel  de la  señal provista  por el  micrófono de
  monitoreo en  el interior de  la cámara} se  controla mediante el  \emph{fader} asociado al  \textbf{canal 1},  la
  perilla  \textsf{TRIM}  asociada  al  \textbf{canal  1}  y  el  \emph{fader}  de  pre-mezcla  general  \textsf{CTL
  ROOM/SUBMIX}.
\end{enumerate}

\emph{\underline{Aclaración:} por razones  de  seguridad,  se  recomienda  colocar  todos  los  faders en su extremo
inferior cuando quieran realizarse maniobras de conexión o desconexión de micrófonos o auriculares.}

%===================================================================================================================
%                                                    APÉNDICE A
%===================================================================================================================
\newpage
\renewcommand\thefigure{1.A.\arabic{figure}}
\setcounter{figure}{0}
%\renewcommand\thesection{A.\arabic{section}}
%\setcounter{section}{0}
\section*{Apéndice 1.A. Configuración de los sonidos propios de \emph{Windows}}
\addcontentsline{toc}{section}{Apéndice 1.A: Configuración de los sonidos propios de \emph{Windows}}
\label{ap1a}
En  este apéndice  se describirá  cómo deshabilitar  los sonidos  propios del  funcionamiento del  sistema operativo
\emph{Windows} XP Professional, versión 2002, service pack 3.
%-------------------------------------------------------------------------------------------------------------------
\subsection*{Instrucciones}
\begin{enumerate}
\item  Dirigirse al  botón menú  inicio  \includegraphics[height=0.015  \textheight]{ap1a_1.png},  localizado  en el
extremo inferior izquierdo de la pantalla principal.
\item  Acceder luego  al \emph{Panel  de control}  de \emph{Windows},  en  el cual  podrá realizar  la configuración
deseada.  Entre todas  las opciones,  buscar la  opción \textsf{Dispositivos de  sonido y audio},  la  cual posee el
icono \includegraphics[height=0.015 \textheight]{ap1a_2.png}.
\item A continuación,  se abrirá una ventana como la que se muestra en la Fig.  \ref{ap1a-3}, donde se ha presionado
previamente  en  la  pestaña  \textsf{Sonidos}.   Luego,  elegir  la  opción  \textsf{Sin  sonidos}  sobre  el  menú
\textsf{Combinación de sonidos}.
\item A continuación,  presionar el botón \textsf{Aplicar}  y,  por último,  el botón \textsf{Aceptar} --ambos en el
extremo inferior de la ventana mostrada en la Fig.  \ref{ap1a-3}--.
\end{enumerate}

\begin{figure}[!b]
  \centering
  \includegraphics[height=0.35\textheight]{ap1a_3.png}\\
  \caption{Ventana de configuración de los sonidos del sistema operativo.} \label{ap1a-3}
\end{figure}

%===================================================================================================================
%                                                    APÉNDICE B
%===================================================================================================================
%\newpage
\renewcommand\thefigure{1.B.\arabic{figure}}
\setcounter{figure}{0}
%%\renewcommand\thesection{B.\arabic{section}}
%%\setcounter{section}{0}
\section*{Apéndice 1.B: Configuración de placa de sonido en \text{\emph{Windows}}}
\addcontentsline{toc}{section}{Apéndice 1.B: Configuración de placa de sonido en \emph{Windows}}
\label{ap1b}
En este apéndice se  describirá cómo realizar la configuración del micrófono,  de manera  que la señal capturada por
éste pueda enviarse a  través  de  la  salida  de  la  placa  de  sonido  en  el sistema operativo \emph{Windows} XP
Professional, versión 2002, service pack 3.
\subsection*{Instrucciones}
\begin{enumerate}
\item Localizar  el icono \textsf{Volumen}  en el área  de notificación ubicada  en la esquina  inferior derecha del
escritorio del sistema operativo (ver ref.  \textcircled{\scriptsize 1} de la Fig.  \ref{scsh1}).
\item Presionar  el botón  derecho del  ratón sobre  el icono  de volumen  y,  a continuación,  presionar  la opción
`\textsf{Abrir controles de volumen}' (ver ref.  \textcircled{\scriptsize 2} de la Fig.  \ref{scsh1}).
\end{enumerate}

\begin{figure}[!b]
  \centering
  \includegraphics[height=0.20\textheight]{screenshot1.png}
  \caption{\emph{Referencias:}  \textcircled{\scriptsize  1}   Ubicación  del  icono  de  volumen   en  el  área  de
  notificación.  \textcircled{\scriptsize 2} Menú desplegado al presionar botón  derecho del ratón sobre el icono de
  volumen.  \textcircled{\scriptsize 3} Ventana  de control de volumen de  los \emph{dispositivos de reproducción}.}
  \label{scsh1}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{2}
\item Se abrirá  una ventana denominada \textsf{Control de  volumen} sobre la que podrá visualizar  el estado de los
distintos  \emph{faders}  de  control  del audio  reproducido  por  el  sistema.  En  particular,  verificar  que el
\emph{fader} de  \textsf{Control de volumen}  (ver extremo izquierdo  de la ref.  \textcircled{\scriptsize  3} de la
Fig.   \ref{scsh1})  y   el  \emph{fader}   \textsf{Volumen  de  micrófono}   (ver  extremo   derecho  de   la  ref.
\textcircled{\scriptsize 3}  de la Fig.  \ref{scsh1})  no se encuentren  en estado  de \textsf{Silencio}  y,  de ser
necesario, ajustarlos a un valor requerido para la correcta comunicación.
\end{enumerate}

\begin{figure}[!b]
  \centering
  \includegraphics[height=0.20\textheight]{screenshot2.png}
  \caption{\emph{Referencias:} \textcircled{\scriptsize 4}  Acceso a las propiedades de  reproducción y grabación de
  sonido  en  \emph{Windows}.  \textcircled{\scriptsize  5}  Ventana  de  configuración  de  las  propiedades  de la
  reproducción y grabación de sonido en \emph{Windows}.} \label{scsh2}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{3}
  \item  Sobre  la esquina  superior  izquierda de  la  ventana  \textsf{Control  de  volumen},  presionar  el botón
  \textsf{Opciones} y luego \textsf{Propiedades} (ver ref.  \textcircled{\scriptsize 4} de la Fig.  \ref{scsh2}).
  \item Se  desplegará una ventana en  la cual podrá habilitar  de manera  individual qué  \emph{faders} verá  en la
  ventana  \textsf{Control  de  volumen}  de reproducción  en  caso  que  la  opción  \textsf{Dispositivo mezclador}
  corresponda  con   \textsf{Conexant  HD  Audio  output}   (ver  ref.   \textcircled{\scriptsize  5}   de  la  Fig.
  \ref{scsh2}).
  \item Para observar el  estado  de  los  \emph{faders}  asociados  a  los  dispositivos  de grabación o entrada de
  audio--el micrófono  del operador,  en  este caso--  seleccionar \textsf{Conexant  HD Audio  input} sobre  el menú
  \textsf{Dispositivo  mezclador} y  presionar \textsf{Aceptar}  (ver ref.  \textcircled{\scriptsize  6} de  la Fig.
  \ref{scsh3}).
  \item Al  presionar \textsf{Aceptar},  aparecerá una ventana  denominada \textsf{Control de grabación}  en la cual
  podrá  ver  el  estado   de   los   \emph{faders}   de   los   dispositivos   de   entrada   de  audio  (ver  ref.
  \textcircled{\scriptsize 7}  de la Fig.  \ref{scsh3}).  En  particular,  marcar la opción  \textsf{Seleccionar} en
  el canal \textsf{Micrófono} y colocar el \emph{fader} asociado en una posición cercana a su extremo superior.
\end{enumerate}

\begin{figure}[!t]
  \centering
  \includegraphics[height=0.2\textheight]{screenshot3.png}
  \caption{\emph{Referencias:}  \textcircled{\scriptsize  6}  Ventana de  configuración  de  las  propiedades  de la
  grabación y reproducción  de sonido en \emph{Windows}.  \textcircled{\scriptsize 7} Ventana  de control de volumen
  de los \emph{dispositivos de grabación}.} \label{scsh3}
\end{figure}
%===================================================================================================================
%===================================================================================================================
%===================================================================================================================
% 'Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>'
% 2020-02-14
%===================================================================================================================
%===================================================================================================================
