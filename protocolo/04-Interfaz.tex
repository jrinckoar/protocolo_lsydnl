%===================================================================================================================
%===================================================================================================================
%                                 PROTOCOLO PARA LA ADQUISICIÓN Y EL ALMACENAMIENTO
%                                DE SEÑALES BIOMÉDICAS ASOCIADAS A LA ACTIVIDAD VOCAL
%-------------------------------------------------------------------------------------------------------------------
%                       CAPÍTULO CUATRO: CONSIDERACIONES SOBRE LA DISPOSICIÓN DEL EQUIPAMIENTO
%                        EN EL INTERIOR DE LA CÁMARA ANECOICA Y SU INTERACCIÓN CON EL SUJETO
%-------------------------------------------------------------------------------------------------------------------
%...................................................................................................................
% Autores:
%   Ariel Stassi <aestassi@ingenieria.uner.edu.ar>
%   Gabriel Alzamendi <galzamendi@ingenieria.uner.edu.ar>
%   Juan Manuel Miramont <jmiramont@ingenieria.uner.edu.ar>
%   Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>
% 2020-02-14
%===================================================================================================================
%===================================================================================================================
\pagestyle{main}
\chapter{Consideraciones sobre la disposición del equipamiento en el interior de la cámara anecoica y su interacción
con el sujeto}
\label{proto3}

%===================================================================================================================
%                                                     OBJETIVOS
%===================================================================================================================
\section{Objetivos}
Los objetivos principales de este documento son:
\begin{itemize}
  \setlength{\itemsep}{0.5pt}
  \item Realizar  una lista de  acciones encaminadas a  la correcta ubicación  del equipamiento para  el registro de
  señales biomédicas --en particular, voz y electroglotograma-- en el interior de la cámara anecoica.
  \item Proveer un método secuencial para la disposición del \emph{hardware} respecto del sujeto,  en el interior de
  la cámara anecoica, que permita la correcta adquisición simultánea de señales de voz y electroglotograma.
  \item Establecer recomendaciones importantes  que el sujeto deberá tener en cuenta  durante \emph{todo} el proceso
  de adquisición.
\end{itemize}

%===================================================================================================================
%                                                      ALCANCE
%===================================================================================================================
\section{Alcance}
El presente  documento ha sido  desarrollado para su utilización  en las instalaciones  de la cámara  anecoica de la
\fiuner,  con instrumentos provistos por  el Laboratorio de Señales y Dinámicas No  Lineales también perteneciente a
dicha institución.  La cámara anecoica de la \fiuner\ cuenta con dos recintos:  la cámara anecoica propiamente dicha
y la sala de comandos asociada.

En este contexto,  se ha diseñado este instructivo para la correcta disposición del \emph{hardware} y los accesorios
en el  interior de la cámara,  así  como la correcta ubicación  del sujeto respecto  a éstos.  Además,  se presentan
recomendaciones importantes a tener en cuenta por parte del sujeto en cuanto al equipamiento,  durante el proceso de
adquisición.  La secuencia de acciones  presentada en este documento se confeccionó bajo la  suposición de que ya se
han llevado a cabo íntegramente \emph{todas} las  instrucciones propuestas en los documentos ``Instrucciones para la
conexión del equipamiento de  comunicación sujeto-operador'' e ``Instrucciones para la  conexión y configuración del
equipamiento  de adquisición'',  situados  respectivamente en  las pág.  \pageref{proto1}  y \pageref{proto2}  de la
presente protocolo.

Se recomienda contar  con \emph{dos} operadores para la  ejecución de algunas acciones dentro  de este apartado,  en
particular aquéllas que  requieren de un proceso de  ajuste por ``prueba y error''.  De  aquí en adelante,  mediante
\textsf{Operador-S}  haremos  referencia  al  operador  que  se   encuentra  en  la  sala  de  comandos  y  mediante
\textsf{Operador-I}    al   operador    que   se    encuentra   en    el   interior    de   la    cámara   anecoica.
\emph{\underline{Aclaración:} salvo que se especifique lo contrario,  se supone que las acciones aquí listadas serán
ejecutadas por el} \textsf{Operador-I}.

%===================================================================================================================
%                                               MATERIALES NECESARIOS
%===================================================================================================================
\section{Materiales necesarios}
\begin{itemize}
  \setlength{\itemsep}{0.5pt}
  \item  1  sistema  de  adquisición  \biopac\  MP150  con  los  módulos  accesorios  UIM100C,  DA100C  y TCI115,  y
  \emph{software} de comando asociado \emph{AcqKnowledge 4.2};
  \item 1 electroglotógrafo \laryngograph\ EGG-A100, con cable paciente y cable de envío de señal incluidos;
  \item 1 micrófono dinámico de alta fidelidad con conexión de salida XLR3 macho;
  \item 1 pie de micrófono con soporte acorde al micrófono disponible;
  \item 1 mesa;
  \item 1 silla.
\end{itemize}

%===================================================================================================================
%                                               NOCIONES PRELIMINARES
%===================================================================================================================
\section{Nociones preliminares}
Diversos estudios han demostrado que, para la obtención de registros de calidad, no sólo resulta de suma importancia
el control de las condiciones del entorno y la correcta configuración de los parámetros de adquisición en los medios
tecnológicos  empleados,  sino también  la correcta  ubicación y  comportamiento del  sujeto durante  \emph{todo} el
proceso de adquisición.
%-------------------------------------------------------------------------------------------------------------------
\subsection{Caracterización de las señales de interés}
El  objetivo general  de esta  serie de  documentos es  sistematizar el  proceso de  registro de  señales biomédicas
asociadas a la actividad vocal, en particular, la \emph{señal de voz} y la \emph{señal de electroglotograma}.  Ambas
señales se generan durante el funcionamiento del \emph{aparato fonador} de un individuo (para mayor información, ver
el Apéndice  A de este  apartado,  situado en la  pág.  \pageref{apendice_A}).  De esta  manera,  a partir  de estas
señales puede extraerse información acerca de este sistema.  En este contexto,  podemos decir que la señal de voz es
la onda acústica resultante del proceso de fonación emitida desde de los labios y, en algunos casos, desde las fosas
nasales (ver ref.  \textcircled{\scriptsize 8} y  \textcircled{\scriptsize 9} de la Fig.  \ref{aparatofonador}).  En
particular,  para realizar  su registro emplearemos  un micrófono de  alta fidelidad en  conjunto con el  sistema de
adquisición \biopac.  En  la Fig.  \ref{voz} puede  verse la morfología típica  correspondiente a la  emisión de una
vocal \textsl{/a/} sostenida, capturada por un micrófono de alta fidelidad.

\begin{figure}[t!]
\centering
  \includegraphics[width=0.95\textwidth]{voz.pdf}
  \caption{Señal de voz correspondiente a la fonación  de una vocal \textsl{/a/} sostenida.  La escala temporal está
  presentada en milisegundos,  $ms$,  y la  escala  de  la  magnitud  de  la  señal  de voz en unidades arbitrarias,
  U.A..}\label{voz}
\end{figure}

Por otro lado, la señal de electroglotograma --en adelante, abreviada como EGG-- aporta información relacionada a la
actividad de las cuerdas  vocales durante la producción de la voz (ver  ref.  \textcircled{\scriptsize 5} de la Fig.
\ref{aparatofonador}).  Para la adquisición  de esta señal emplearemos un  dispositivo denominado electroglotógrafo,
el cual  captura el patrón  de variación temporal del  área de contacto  entre las cuerdas  vocales,  ubicadas en el
interior  de  la  laringe  (ver  ref.   \textcircled{\scriptsize   5}  y  \textcircled{\scriptsize  B}  de  la  Fig.
\ref{aparatofonador}).  En la Fig.  \ref{EGG}  puede verse la morfología  de la señal de  EGG,  correspondiente a la
emisión  de   una  vocal   \textsl{/a/}  sostenida,   capturada   por  un   electroglotógrafo  con   los  electrodos
\emph{correctamente} ubicados  en la  superficie anterior  del cuello  del sujeto  (para mayor  información,  ver el
Apéndice B de este apartado, situado en la pág.  \pageref{ap_colocacionelectrodos}).

\begin{figure}[b!]
\centering
  \includegraphics[width=0.95\textwidth]{EGG.pdf}
  \caption{Señal de EGG correspondiente a la fonación  de una vocal \textsl{/a/} sostenida.  La escala temporal está
  presentada en milisegundos,  $ms$,  y la  escala  de  la  magnitud  de  la  señal  de EGG en unidades arbitrarias,
  U.A..}\label{EGG}
\end{figure}

%===================================================================================================================
%                                                   INSTRUCCIONES
%===================================================================================================================
\section{Instrucciones}
Previo a ingresar al interior de la cámara,  el  sujeto y \textsf{Operador-I} deberán apagar sus teléfonos móviles y
toda otra  fuente de ruido electromagnético  --ej.,  \emph{tablets,  notebooks}-- o de  perturbación acústica --ej.,
sistemas portátiles de audio--.

\begin{enumerate}
  \item  Ubicar  la   mesa  en  el  centro   de  la  cámara  anecoica  (ver   ref.   \textcircled{\scriptsize  1}  y
  \textcircled{\scriptsize 2} de la Fig.  \ref{layout}).
\end{enumerate}

\begin{figure}[b!]
\centering
  \includegraphics[width=0.73\textwidth]{camara2_labels.pdf}
  \caption{Disposición del mobiliario  y el equipamiento en el interior  de la cámara anecoica.  \emph{Referencias:}
  \textcircled{\scriptsize 1}  Mesa.  \textcircled{\scriptsize  2}  Puerta.  \textcircled{\scriptsize  3} Sistema de
  adquisición \biopac.  \textcircled{\scriptsize  4}  Electroglotógrafo  \laryngograph.  \textcircled{\scriptsize 5}
  Micrófono   de   monitoreo   y   soporte   asociado.    \text{\textcircled{\scriptsize   6}   Silla   ergonómica.}
  \textcircled{\scriptsize 7} Micrófono de adquisición y soporte asociado.}\label{layout}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{1}
  \item Disponer el conjunto  de módulos \biopac\ --MP150,  UIM100C,  DA100C y accesorios--  en la esquina posterior
  izquierda  de  la  mesa (ver  ref.  \textcircled{\scriptsize  3}  de  la  Fig.  \ref{layout}).  Colocar  el equipo
  \laryngograph\  EGG-A100 en  la esquina  opuesta,  es decir,  la  esquina anterior  derecha de  la mesa  (ver ref.
  \textcircled{\scriptsize 4} de la Fig.  \ref{layout}).
  \item Ubicar el micrófono de \emph{monitoreo},  asociado a su soporte,  cercano a la esquina anterior izquierda de
  la mesa (ver ref.  \textcircled{\scriptsize 5} de la Fig.  \ref{layout}).
  \item El sujeto deberá  realizar  las  emisiones  vocales  que  se  le  soliciten en posición sentado.  Para ello,
  disponer la  silla para  el voluntario al  frente del electroglotógrafo,  reservando  espacio a  la derecha  de la
  silla del  sujeto para la  ubicación del pie del  micrófono de adquisición  (ver ref.  \textcircled{\scriptsize 6}
  de la Fig.  \ref{layout}).
  \item Solicitar al sujeto que se siente con el torso recto.  \label{postura}
  \item Orientar la cápsula --el sensor propiamente dicho-- del micrófono de \emph{monitoreo} hacia el sujeto.
\end{enumerate}

\emph{\underline{Nota:} este equipo cuenta  con tres tamaños diferentes de electrodos y  su elección dependerá de la
contextura  física del  sujeto voluntario  y,  en particular,  del  tamaño  de  la  laringe  del  mismo.  Para mayor
información,   ver  los  Apéndices  A  y  B  de  este  apartado,   situados  en  las  pág.   \pageref{apendice_A}  y
\pageref{ap_colocacionelectrodos}, respectivamente.}

\begin{enumerate}
\setcounter{enumi}{6}
  \item Ubicar  los electrodos  del electroglotógrafo  en su posición  ``óptima'' de  acuerdo a  lo discutido  en el
  Apéndice B, situado en la pág.  \pageref{ap_colocacionelectrodos}.\label{ubicacion}
  \item Solicitar al  \textsf{Operador-S} que realice registros  de prueba de las  señales de Voz y  EGG mientras el
  sujeto realiza la fonación de una vocal \textsl{/a/} sostenida.  \label{registrodeprueba}
  \item Verificar la calidad del posicionamiento de los electrodos, realizado en el paso \ref{ubicacion}, de acuerdo
  a la  señal de EGG registrada  durante el paso \ref{registrodeprueba}.  \text{En  caso de no  obtener el resultado
  esperado, volver al paso \ref{ubicacion}.}
  \item Una vez  que se ha encontrado  la ubicación de los electrodos,  solicitar  al sujeto que los  sostenga en la
  posición deseada y colocar la faja elástica con  abrojos,  la cual sostendrá los electrodos en la posición deseada
  durante el proceso de adquisición.
  \item Ubicar  el pie del  micrófono de \emph{adquisición}  a la derecha  del sujeto y  colocar el micrófono  en el
  sostén correspondiente  (ver ref.  \textcircled{\scriptsize 7}  de la  Fig.  \ref{layout}).  Por último,  orientar
  la cápsula hacia la boca  del sujeto,  de manera que el eje de simetría del  micrófono se encuentre a 45º respecto
  de la horizontal (ver Fig.  \ref{orientacionmic}).  \label{orient_item}
\end{enumerate}

\begin{figure}[b!]
\centering
  \includegraphics[width=0.8\textwidth]{orient_mic.pdf}
  \caption{\text{Orientación  del  micrófono  para  mejorar la  captación  de  sonidos  como  la  \textsl{/p/}  o la
  \textsl{/t/}}.}\label{orientacionmic}
\end{figure}

\begin{enumerate}
\setcounter{enumi}{11}
  \item Establecer una  distancia de $15\ cm$ entre  la boca del sujeto y la  cápsula del micrófono,  conservando la
  orientación  establecida  de  acuerdo  al   paso  \ref{orient_item}.   \emph{\underline{Nota:}  se  debe  asegurar
  distancia constante a lo largo de todo el proceso de adquisición.} \label{distmic}
  \item Verificar que se cumplan las condiciones necesarias durante el proceso de adquisición:
    \begin{itemize}
      \item el micrófono de \emph{monitoreo} se encuentre orientado hacia el sujeto,
      \item el \emph{sujeto} se encuentre \emph{sentado} con el \emph{torso recto},
      \item los \emph{electrodos} se encuentren \emph{correctamente ubicados y sostenidos} por la faja elástica,
      \item el  micrófono de \emph{adquisición}  se encuentre correctamente  ubicado --distancia y  orientación-- de
      acuerdo  a  las   indicaciones   dadas   en   los   pasos   \ref{orient_item}   y   \ref{distmic}  y  la  Fig.
      \ref{orientacionmic}.
    \end{itemize}
  \item Recomendar enfáticamente al sujeto que mantenga la  posición del torso,  así como la distancia y orientación
  de la boca respecto del  micrófono de \emph{adquisición} de acuerdo a lo establecido  en los pasos \ref{postura} y
  \ref{distmic}.
  \item Recomendar  enfáticamente al  sujeto que  permanezca en calma  y lo  más quieto  posible,  debido a  que las
  vibraciones que produzcan  sus  movimientos  pueden  ingresar  \emph{de  manera  no  deseada}  a los registros por
  diferentes ``vías'', entre las cuales podemos mencionar:
    \begin{itemize}
      \item acoplamiento mecánico entre golpes del pie del sujeto sobre el piso y el pie de micrófono,
      \item desplazamiento de los electrodos ocasionados por movimientos de cabeza o cuello del sujeto,
      \item acoplamiento acústico entre golpes realizados por el sujeto  sobre la mesa o sobre su propio cuerpo y el
      transductor del micrófono.
    \end{itemize}
  \item Abandonar el interior de la cámara anecoica y cerrar la puerta correctamente.  \emph{\underline{Aclaración:}
  esta última acción es  llevada a cabo por el} \textsf{Operador-I}\emph{,  quedando el sujeto  en el interior de la
  cámara, en condiciones de comenzar la etapa de adquisición.}
\end{enumerate}

%===================================================================================================================
%                                                    APÉNDICE A
%===================================================================================================================
\newpage
\renewcommand\thefigure{4.A.\arabic{figure}}
\setcounter{figure}{0}
%\renewcommand\thesection{A.\arabic{section}}
%\setcounter{section}{0}
\section*{Apéndice 4.A: El aparato fonador}
\addcontentsline{toc}{section}{Apéndice 4.A: El aparato fonador}
\label{apendice_A}
El aparato fonador de un individuo es el conjunto de  todos los órganos implicados en la producción de la voz.  Como
puede  observarse  en  la Fig.  \ref{aparatofonador},  este  sistema  comprende  una  gran  cantidad  de estructuras
anatómicas.  Tomando la laringe como estructura de referencia, este sistema puede dividirse en tres componentes:
\begin{itemize}
  \item  el  \emph{componente   subglótico}  o  \emph{subsistema  respiratorio},   conformado   por  la  musculatura
  respiratoria,   los  pulmones   y  las   vías  aéreas  inferiores   --árbol  bronquial   y  tráquea--   (ver  ref.
  \textcircled{\scriptsize A} de la Fig.  \ref{aparatofonador}).
  \item la \emph{laringe} o \emph{subsistema laríngeo} es una estructura músculo-cartilaginosa,  situada en la parte
  \emph{anterior}  del  cuello (ver  ref.  \textcircled{\scriptsize  B}  de  la  Fig.  \ref{aparatofonador}).  En su
  interior se alojan las  cuerdas vocales,  entre las cuales queda comprendido  un orificio denominado \emph{glotis}
  (ver  ref.  \textcircled{\scriptsize  5}  de  la  Fig.  \ref{aparatofonador}).  \emph{\underline{Nota:}  desde  el
  exterior,  la localización  de esta estructura  puede estimarse por  palpación del  cartílago tiroides  durante el
  proceso de fonación o deglución} (ver ref.  \textcircled{\scriptsize 4} de la Fig.  \ref{aparatofonador}).
  \item el \emph{tracto vocal  supralaríngeo} o \emph{subsistema articulatorio} está conformado  por las vías aéreas
  superiores,  la  faringe  y  las  cavidades bucal  y  nasal  (ver  ref.  \textcircled{\scriptsize  C}  de  la Fig.
  \ref{aparatofonador}).
\end{itemize}

\begin{figure}[b!]
\centering
  \includegraphics[width=0.4 \textwidth]{aparatofonador.pdf}
  \caption{Aparato fonador del ser humano. \emph{Referencias:} \textcircled{\scriptsize A} Componente subglótico, compuesto por \textcircled{\scriptsize 1} Pulmones. \textcircled{\scriptsize 2} Diafragma. \textcircled{\scriptsize 3} Tráquea y vías aéreas inferiores. \textcircled{\scriptsize B} Laringe, compuesta por \textcircled{\scriptsize 4} Cartílago tiroides. \textcircled{\scriptsize 5} Cuerdas vocales. \textcircled{\scriptsize C} Tracto supralaríngeo, compuesto por \textcircled{\scriptsize 6} Faringe. \textcircled{\scriptsize 7} Lengua. \textcircled{\scriptsize 8} Fosas nasales. \textcircled{\scriptsize 9} Labios.}\label{aparatofonador}
\end{figure}

%===================================================================================================================
%                                                    APÉNDICE B
%===================================================================================================================
\newpage
\renewcommand\thefigure{4.B.\arabic{figure}}
\setcounter{figure}{0}
%\renewcommand\thesection{B.\arabic{section}}
%\setcounter{section}{0}
\section*{Apéndice 4.B: Colocación adecuada de electrodos para electroglotografía}
\addcontentsline{toc}{section}{Apéndice 4.B: Colocación adecuada de electrodos para electroglotografía}
\label{ap_colocacionelectrodos}

La colocación de los electrodos del electroglotógrafo en el sitio adecuado es un proceso de tipo ``prueba y error''.
Como ya se ha  mencionado,  mediante este dispositivo se pretende capturar el  comportamiento de las cuerdas vocales
durante el proceso de fonación.  De esta manera,  los electrodos deberían colocarse a ambos lados de la laringe y lo
más próximos a las cuerdas vocales como sea posible.  En la Fig.  \ref{laringe_cuerdas} se puede observar un esquema
que ilustra la relación anatómica existente,  en un corte transversal\footnote{En Anatomía,  un corte transversal es
un corte imaginario que  permite dividir al cuerpo en dos porciones,  una  \emph{superior} y otra \emph{inferior}.},
entre: la glotis, las cuerdas vocales, el cartílago tiroides y la superficie \emph{anterior} del cuello.

\begin{figure}[b!]
\centering
  \includegraphics[width=0.2 \textwidth]{laringe.pdf}
  \caption{Estructuras anatómicas  en relación a las  cuerdas vocales.  \emph{Referencias:} \textcircled{\scriptsize
  1}  Glotis.   \textcircled{\scriptsize  2}  Cuerdas  vocales.   \textcircled{\scriptsize  3}  Cartílago  tiroides.
  \textcircled{\scriptsize 4} Superficie \emph{anterior} del cuello.}\label{laringe_cuerdas}
\end{figure}

En este sentido,  el objetivo consiste en hallar las estructuras anatómicas de referencia que nos permitan localizar
la laringe y,  de esta manera,  estimar la localización de las cuerdas vocales.  Desde la superficie de la piel,  la
localización de  la laringe puede  llevarse a cabo  mediante la técnica  de \emph{palpación} del  cartílago tiroides
durante el proceso de fonación o deglución (ver ref.  \textcircled{\scriptsize 4} de la Fig.  \ref{aparatofonador} y
ref.  \textcircled{\scriptsize 3} de la Fig.  \ref{laringe_cuerdas}).  \emph{\underline{Nota:} se recomienda colocar
la cabeza  del sujeto en  hiperextensión para visualizar  mejor el cartílago  tiroides.  No solicitar al  sujeto que
realice la fonación o deglución en esta situación.}

Debe tenerse en  cuenta que el cartílago  tiroides consta de dos láminas  cuyos bordes anteriores se  fusionan en un
ángulo medio o  \emph{prominencia laríngea},  la cual puede palparse  desde la superficie anterior del  cuello en un
plano  medio  (ver  ref.  \textcircled{\scriptsize  4}  de  la  Fig.  \ref{laringe_cuerdas}).  Dicho  ángulo  es  de
aproximadamente 90º y 120º en el género masculino y femenino,  respectivamente.  Por otro lado, el espesor de tejido
graso subcutáneo es otro  factor  que  influye  en  este  proceso,  independientemente  del género.  Por esta razón,
generalmente resulta más dificultoso localizar esta estructura en las mujeres.  Tener en cuenta además,  que durante
el proceso  de fonación  en condiciones  fisiológicas,  la posición \emph{vertical}  de la  laringe se  encuentra en
continua variación.

Por las razones mencionadas  anteriormente,  la  colocación  de  los  electrodos  en  el  sitio óptimo es un proceso
iterativo gobernado por la calidad de la señal que  se obtiene en los diferentes intentos.  Más aún,  la posición en
la  cual la  laringe permanecerá  en mayor  medida dependerá  del  tipo  de  emisión  vocal  realizada.  En  la Fig.
\ref{secuencia4},  se muestran cuatro señales de EGG ruidoso correspondientes  a una serie de intentos en los cuales
se ha  ido modificando ligeramente  la posición de  los electrodos,  mientras el  sujeto realiza la  fonación de una
\textsl{/a/} sostenida.  En términos  generales,  el \emph{ruido} es toda información  \emph{no deseada} presente en
una señal de interés  que dificulta su correcta interpretación.  En este caso  particular,  la señal deseada será el
EGG ``limpio'' (ver Fig.  \ref{EGG}) y el ruido se manifestará en forma de oscilaciones de alta frecuencia, montadas
sobre la señal de EGG.  A lo largo  de la secuencia,  desde el registro $\mathrm{EGG_1}$ al $\mathrm{EGG_4}$,  puede
observarse cómo  disminuye paulatinamente  la presencia de  ruido en la  señal y  a su  vez,  cómo se  incrementa la
amplitud de la señal de  interés  conforme  se  alcanza  la  posición  más  favorable  para el registro.  En la Fig.
\ref{EGG} de este apartado, puede observar la morfología ``ideal'' de una señal de EGG asociada a la fonación de una
\textsl{/a/} sostenida.

\begin{figure}[t!]
\centering
  \includegraphics[width=0.8 \textwidth]{secuencia4.pdf}
  \caption{Secuencia de cuatro  registros de EGG correspondientes  a una \textsl{/a/} sostenida,  a lo  largo de los
  cuales  se  ha  realizado  un  proceso  de  ajuste  en   la  posición  de  los  electrodos  sobre  el  cuello  del
  sujeto.}\label{secuencia4}
\end{figure}

Así,  en este contexto  se plantea una solución de compromiso  entre la posición de los electrodos  y el conjunto de
emisiones que  se solicitarán de  acuerdo al protocolo  ``Instrucciones para el  entrenamiento y adquisición  de las
emisiones vocales a solicitar'', situado en la pág.  \pageref{proto4} de la presente serie de instructivos.  De esta
manera,  se busca sólo una posición ``óptima'' --correspondiente  a la fonación de una \textsl{/a/} sostenida-- para
\emph{todo} el conjunto, evitando así el cambio de ubicación de los electrodos ante las distintas solicitaciones.
%===================================================================================================================
%===================================================================================================================
%===================================================================================================================
% 'Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>'
% 2020-02-14
%===================================================================================================================
%z===================================================================================================================
