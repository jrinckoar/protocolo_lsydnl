close all;
clear all;
clc;

N=4;
M=5220;
EGG=zeros(M,N);
name='C:\Users\Ariel\Documents\Facultad\2C2013\PPS\Propio\Registros\Experimentos_11-29\EGG';
T_s=0.02;
t=0:T_s:(M-1)*T_s;
clear T_s;

for i=1:N
    load(strcat(name,num2str(i),'.mat'));
    x=data(:,2);
    x=resample(x,M,length(x));
    if i==1 || i==4 || i==5
        x=5*x;
    end;
        
    EGG(:,i)=x/10;
    subplot(N,1,i);
    plot(t(1:length(t)/4), EGG(1:length(t)/4,i),'r');
    axis([0 t(M/4) -0.2 0.2]);
%     set(gca,'xTick',[]);
    ylabel(strcat('EGG_',num2str(i),', U.A.'));
    if i==N
        xlabel('tiempo, ms');
    end;
end;