close all; 
clear all; 
clc; 

load('C:\Users\Ariel\Documents\Facultad\2C2013\PPS\Propio\Registros\Experimentos_04-10\Felipe_a_04_granEGG.mat');
N=length(data(:,1));
t=0:isi:isi*(N-1); %definido en ms
Voz=data(:,1);
EGG=data(:,2);

ti=3400; tf=3500;

t_G=ti-ti:isi:tf-ti;
Voz_G=Voz(ti/isi:tf/isi)/10;
EGG_G=EGG(ti/isi:tf/isi)/10;

subplot(2,1,1);
plot(t_G,Voz_G);
ylabel('Se�al de Voz, U.A.');
axis([0 tf-ti mean(Voz_G)-0.35 mean(Voz_G)+0.4]);
xlabel('tiempo, ms')
% set(gca,'xTick',[]);
    

subplot(2,1,2);
plot(t_G,EGG_G,'r');
ylabel('Se�al de EGG, U.A.');
axis([0 tf-ti mean(EGG_G)-0.4 mean(EGG_G)+0.45])
xlabel('tiempo, ms')
